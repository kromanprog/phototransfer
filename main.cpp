#include <QGuiApplication>
#include "qguiqmlapplication.h"
#include "qttoqmlvariant.h"

int main(int argc, char *argv[])
{
    QGuiQmlApplication app(argc,argv);
    if (!app.Init())
        return -1;

    qmlRegisterType<QtToQmlVariant>("kromanprog.types",0,1,"QmlVariant");

    return app.exec();
}

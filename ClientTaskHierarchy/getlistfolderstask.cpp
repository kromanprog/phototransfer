#include "ClientTaskHierarchy/getlistfolderstask.h"
#include <QDataStream>
#include <QByteArray>

GetListFoldersTask::GetListFoldersTask(TaskState ts, Connector *connector,  QMap<QUuid,IClientTask*> *map,  QtToQmlConnector *qmlconn, QObject *parent) : IClientTask(ts,connector,map,qmlconn,this,parent)
{

}

void GetListFoldersTask::Req(QString &req_string)
{
    QByteArray *ba = new QByteArray;
    QDataStream out(ba,QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_9);
    out << ":"+ID.toByteArray() << ":get folders:";
    conn->sendData(*ba);
    delete ba;
}

void GetListFoldersTask::Process(QString str_response)
{
    QStringList list = str_response.split(':');
    QString response = list[2];
    try
    {
        QUuid *guid = new QUuid(list[1]);
        if (ID == (*guid) )
        {
            QStringList lst = response.split(';');
            IClientTask::paramsForQML.clear();
            emit sendToQMLAction("clearFoldersButtons",IClientTask::paramsForQML);

            for (int i=0;i<lst.count();i++)
            {
                IClientTask::paramsForQML.clear();
                IClientTask::paramsForQML.push_back(lst[i]);
                emit sendToQMLAction("appendFolderButton",IClientTask::paramsForQML);
                //emit appendFolderButton(lst[i]);
            }
        }
        if (guid)
            delete guid;
    } catch (...)
    {

    }
    this->Destroy();
}

void GetListFoldersTask::Destroy()
{
    IClientTask::RemoveFromList();
    delete this;
}

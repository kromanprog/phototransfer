#ifndef GETLISTFOLDERSTASK_H
#define GETLISTFOLDERSTASK_H

#include <QObject>
#include "ClientTaskHierarchy/clienttask.h"
#include "QtToQmlConnector.h"

class GetListFoldersTask : public IClientTask
{
    Q_OBJECT
protected:
    //bool GetGuidFromResponse(QString, QUuid*);

signals:
    void clearFoldersButtons();
    void appendFolderButton(QString str);


public:
    GetListFoldersTask(TaskState, Connector*, QMap<QUuid,IClientTask*> *,  QtToQmlConnector *, QObject *parent = nullptr);
    virtual void Req(QString &) override;
    virtual void Process(QString) override;
    virtual void Destroy() override;
};
#endif // GETLISTFOLDERSTASK_H

#ifndef CLIENTTASK_H
#define CLIENTTASK_H

#include <QObject>
#include "connector.h"
#include <QUuid>
#include <QMap>
#include "QtToQmlConnector.h"
#include "QStringList"

enum class TaskState {Wait,Finished};

class IClientTask : public QObject
{
    Q_OBJECT
protected:
    Connector* conn;
    TaskState state;
    QUuid ID;
    QMap<QUuid,IClientTask*> *map;
    QtToQmlConnector *qmlConnector;
    QStringList paramsForQML;

public:
    explicit IClientTask(TaskState, Connector*, QMap<QUuid,IClientTask*> * , QtToQmlConnector  *, IClientTask *derivedObject, QObject *parent = nullptr);
    virtual void Req(QString& req_str) = 0;
    bool operator == (IClientTask&);
    ~IClientTask();
    virtual void Destroy() = 0;
    void RemoveFromList();
    QUuid getID();

signals:
    void sendToQMLAction(QString,QStringList);

public slots:
    virtual void Process(QString) = 0;
};




//class GetListFoldersTask : public IClientTask
//{
//    Q_OBJECT
//protected:
//    //bool GetGuidFromResponse(QString, QUuid*);

//signals:
//    void clearFoldersButtons();
//    void appendFolderButton(QString str);


//public:
//    GetListFoldersTask(TaskState, Connector*, QMap<QUuid,IClientTask*> *,  QQmlContext *, QObject *parent = nullptr);
//    virtual void Req(QString &) override;
//    virtual void Process(QString) override;
//    virtual void Destroy() override;
//};


//class CreateFolderTask : public IClientTask
//{
//    Q_OBJECT
//private:

//signals:


//public:
//    CreateFolderTask(TaskState, Connector*,  QMap<QUuid,IClientTask*> *,  QObject *parent = nullptr);
//    virtual void Req(QString &) override;
//    virtual void Process(QString) override;
//};


#endif // CLIENTTASK_H

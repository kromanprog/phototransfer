#include "ClientTaskHierarchy/clienttask.h"
#include <QDataStream>
#include <QByteArray>

IClientTask::IClientTask(TaskState st, Connector* tcpconnector,QMap<QUuid,IClientTask*>* task_map, QtToQmlConnector *qmlconn,IClientTask *derivedObject, QObject *parent) : QObject(parent)
{
    state = st;
    map = task_map;
    ID = QUuid::createUuid();

    conn = tcpconnector;
    connect(conn,SIGNAL(dataReadyToRead(QString)),this,SLOT(Process(QString)));

    qmlConnector = qmlconn;
    connect(derivedObject,SIGNAL(sendToQMLAction(QString,QStringList)),qmlConnector,SLOT(acceptCommand(QString,QStringList)));
}

bool IClientTask::operator ==(IClientTask &arg)
{
    if (this->ID == arg.ID)
        return true;
    else
        return false;

}

IClientTask::~IClientTask()
{
    disconnect(conn,SIGNAL(dataReadyToRead(QString)),this,SLOT(Process(QString)));
    disconnect(this,SIGNAL(sendToQMLAction(QString,QStringList)),qmlConnector,SLOT(acceptCommand(QString,QStringList)));
}

void IClientTask::RemoveFromList()
{

    if (map->contains(ID))
    {
        map->remove(ID);
    }
}


QUuid IClientTask::getID()
{
    return ID;
}

//GetListFoldersTask::GetListFoldersTask(TaskState ts, Connector *connector,  QMap<QUuid,IClientTask*> *map,  QQmlContext *qmlContext, QObject *parent) : IClientTask(ts,connector,map,qmlContext,parent)
//{

//}

//void GetListFoldersTask::Req(QString &req_string)
//{
//    QByteArray *ba = new QByteArray;
//    QDataStream out(ba,QIODevice::WriteOnly);
//    out.setVersion(QDataStream::Qt_5_9);
//    out << ":"+ID.toByteArray() << ":get folders:";
//    conn->sendData(*ba);
//    delete ba;
//}

//void GetListFoldersTask::Process(QString str_response)
//{
//    QStringList list = str_response.split(':');
//    QString response = list[2];
//    try
//    {
//        QUuid *guid = new QUuid(list[1]);
//        if (ID == (*guid) )
//        {
//            QStringList lst = response.split(';');
//            emit clearFoldersButtons();
//            for (int i=0;i<lst.count();i++)
//            {
//                emit appendFolderButton(lst[i]);
//            }
//        }
//        if (guid)
//            delete guid;
//    } catch (...)
//    {

//    }
//    this->Destroy();
//}

//void GetListFoldersTask::Destroy()
//{
//    IClientTask::RemoveFromList();
//    delete this;
//}




//CreateFolderTask::CreateFolderTask(TaskState ts, Connector *external_connector,  QMap<QUuid,IClientTask*> *map, QObject *parent) : IClientTask(ts,external_connector,map,parent)
//{

//}

//void CreateFolderTask::Req(QString& req_string)
//{
//    QByteArray *ba = new QByteArray;
//    QDataStream out(ba,QIODevice::WriteOnly);
//    out.setVersion(QDataStream::Qt_5_9);
//    out << ":"+ID.toByteArray()+":create folder:" +  (req_string.toUtf8()).trimmed();
//    conn->sendData(*ba);
//    delete ba;
//}

//void CreateFolderTask::Process(QString)
//{
//    GetListFoldersTask *glft = new GetListFoldersTask(TaskState::Wait,this->conn,this->map,nullptr);
//    QString gag = "";
//    glft->Req(gag);
//}

import QtQuick 2.0
import QtMultimedia 5.4

Rectangle {
    id : cameraUI

    width: 800
    height: 480

    color: "black"
    state: "PhotoCapture"

    function setMaxResulotion(camera){
        var res = camera.supportedViewfinderResolutions();
        camera.viewfinder.resolution.width = res[res.length-1].width;
        camera.viewfinder.resolution.height = res[res.length-1].height;

        for(var i=0;i<res.length;i++)
        {
            if(res[i].width>camera.viewfinder.resolution.width)
            {
                camera.viewfinder.resolution = Qt.size(res[i].width,res[i].height)
            }
            else if(res[i].width===camera.viewfinder.resolution.width){
                if(res[i].height>camera.viewfinder.resolution.height)
                {
                    camera.viewfinder.resolution = Qt.size(res[i].width,res[i].height);
                }
            }
        }
    }

    Connections {
        target: MainApplication
        onSendToCam : {
            result.text = value
            result.visible = true
        }
        //         onResizeMainRectangle :
        //         {
        //             cameraUI.width = valueWidth
        //             viewfinder.width = valueWidth
        //             cameraUI.height = valueHeight
        //             viewfinder.height = valueHeight
        //         }
    }

    Component.onCompleted: setMaxResulotion(camera)

    states: [
        State {
            name: "PhotoCapture"
            StateChangeScript {
                script: {
                    camera.captureMode = Camera.CaptureStillImage
                    camera.start()
                }
            }
        },
        State {
            name: "PhotoPreview"
        },
        State {
            name: "VideoCapture"
            StateChangeScript {
                script: {
                    camera.captureMode = Camera.CaptureVideo
                    camera.start()
                }
            }
        },
        State {
            name: "VideoPreview"
            StateChangeScript {
                script: {
                    camera.stop()
                }
            }
        }
    ]

    Camera {
        id: camera
        captureMode: Camera.CaptureStillImage

        imageCapture {
            onImageCaptured: {
                //photoPreview.source = preview
                //stillControls.previewAvailable = true
                //cameraUI.state = "PhotoPreview"
            }
            onImageSaved: {
                MainApplication.setImagePath(camera.imageCapture.capturedImagePath);
                //MainApplication.sendFileToServer(camera.imageCapture.capturedImagePath)
                camera.unlock()
            }
        }

        videoRecorder {
            //resolution: "640x480"
            frameRate: 30
        }
    }

    PhotoPreview {
        id : photoPreview
        anchors.fill : parent
        onClosed: cameraUI.state = "PhotoCapture"
        visible: cameraUI.state == "PhotoPreview"
        focus: visible
    }

    VideoPreview {
        id : videoPreview
        anchors.fill : parent
        onClosed: cameraUI.state = "VideoCapture"
        visible: cameraUI.state == "VideoPreview"
        focus: visible

        //don't load recorded video if preview is invisible
        source: visible ? camera.videoRecorder.actualLocation : ""
    }

    VideoOutput {
        id: viewfinder
        visible: cameraUI.state == "PhotoCapture" || cameraUI.state == "VideoCapture"

        x: 0
        y: 0
        width: parent.width - stillControls.buttonsPanelWidth
        height: parent.height

        source: camera
        autoOrientation: true
    }

    PhotoCaptureControls {
        id: stillControls
        anchors.fill: parent
        camera: camera
        visible: cameraUI.state == "PhotoCapture"
        onPreviewSelected: cameraUI.state = "PhotoPreview"
        onVideoModeSelected: cameraUI.state = "VideoCapture"
    }

    VideoCaptureControls {
        id: videoControls
        anchors.fill: parent
        camera: camera
        visible: cameraUI.state == "VideoCapture"
        onPreviewSelected: cameraUI.state = "VideoPreview"
        onPhotoModeSelected: cameraUI.state = "PhotoCapture"
    }


}

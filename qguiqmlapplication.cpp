#include "qguiqmlapplication.h"
#include "appmanager.h"

QGuiQmlApplication::QGuiQmlApplication(int &argc, char **argv) : QGuiApplication(argc,argv)
{

}

QGuiQmlApplication::~QGuiQmlApplication()
{
    delete engine;
    delete iManager;
    delete appmanager;
}

bool QGuiQmlApplication::Init()
{
    engine = new QQmlApplicationEngine();
    engine->load(QUrl(QLatin1String("qrc:/photoTransfer.qml")));
    if (engine->rootObjects().isEmpty())
        return false;

    context = engine->rootContext();
    iManager = new ImageManager();
    appmanager = new AppManager();

    qtqmlconnector = new QtToQmlConnector(context,appmanager);


    connect(qtqmlconnector,SIGNAL(requestImageFileName(int)),iManager,SLOT(getFileName(int)));
    connect(iManager,SIGNAL(fileNameResponse(QString,QStringList)),qtqmlconnector,SLOT(acceptCommand(QString,QStringList)));
    connect(qtqmlconnector,SIGNAL(toQtFolderListUpdate()),appmanager,SLOT(folderListUpdate()));
    return true;
}

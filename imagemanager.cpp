#include "imagemanager.h"
#include <cassert>
#include <QDir>
#include <QFileInfoList>
#include <QFileInfo>
#include <QDebug>


void ImageManager::findImagesRecursively(QString rootDir)
{
    QDir root;
    root.setPath(rootDir);
    QFileInfoList itemList = root.entryInfoList();
    QFileInfo fileInfo;
    for (QFileInfo fileInfo : itemList)
    {
        if (fileInfo.isDir())
        {
            if ((fileInfo.fileName() != "..") && (fileInfo.fileName() != "."))
            {
                findImagesRecursively(fileInfo.absoluteFilePath());
            }
        } else {
            if ((fileInfo.completeSuffix() == "jpg") and (rootDir.contains("DCIM") or rootDir.contains("Camera") ))
            {
                urlList.append("file:///"+fileInfo.absoluteFilePath());
            }
        }
    }
}

ImageManager::ImageManager(QObject *parent) : QObject(parent)
{
    findImagesRecursively("/storage");
}

void ImageManager::getFileName(int index)
{
    //assert(index < 0);
    if ((index >= 0) and (index < urlList.length()))
    {
        QStringList resp;
        resp.append(urlList[index]);
        emit fileNameResponse("sendFileNameResponse",resp);
    }
}

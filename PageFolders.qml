import QtQuick 2.0
import kromanprog.types 0.1
import "Scripts.js" as Scripts


Item {

    //property QmlVariant parametr:

    Connections {
        target: MainApplication
    }

    ListView {
        model : FolderList
        spacing: 2
        delegate: Text {
            text : modelData
        }
    }

    QmlVariant {
        id : parameter
        string_param_1: ""
    }


    Component.onCompleted: {
        //Scripts.reset(parametr)
        //        parametr.string_parametr_1 = ""
//        parametr.string_parametr_2 = ""
//        parametr.string_parametr_3 = ""
//        parametr.int_parametr_1 = -1;
//        parametr.int_parametr_2 = -1;
//        parametr.int_parametr_3 = -1;
        //parametr.string_param_1 = "test string"
        parameter.reset()
        parameter.pushString("test string")
        MainApplication.acceptCommandFromQML("GetFolderList",parameter)
        //MainApplication.requestFromQMLWithoutArgs()
    }

}

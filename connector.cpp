#include "connector.h"


Connector::Connector(const QString &ipaddress, int port)
{
  serverAddress.setAddress(ipaddress);
  serverPort = port;
  soc = new QTcpSocket();
  connect(soc,SIGNAL(readyRead()),this,SLOT(readData()));

}

Connector::~Connector()
{
    if (soc)
        delete soc;
}

bool Connector::sendData(QByteArray& ba) const
{
    try
       {
           soc->connectToHost(serverAddress,serverPort);
           soc->write(ba);
           return true;
       } catch(...)
       {
           return false;
       }
}

void Connector::readData()
{
    QTextStream in(soc);
    in.setCodec("UTF-8");
    QString qs,buff;
    while (!in.atEnd())
    {
        in >> buff;
         qs.append(' ');
         qs.append(buff);
    }
    emit dataReadyToRead(qs);
   buff.clear();
   qs.clear();
}

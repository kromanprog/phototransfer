#ifndef QTTOQMLCONNECTOR_H
#define QTTOQMLCONNECTOR_H

#include <QObject>
#include <QQmlContext>
#include <QMap>
#include <appmanager.h>
#include "qttoqmlvariant.h"
#include "ExecuterHierarchy/SimpleExecuter.h"

class QtToQmlConnector : public QObject
{
    Q_OBJECT

    typedef void (QtToQmlConnector::*ProcessingWithoutArgumets)();
    typedef void (QtToQmlConnector::*ProcessingWithOneArgumet)(QString);
    typedef void (QtToQmlConnector::*ProcessingWithTwoArgumets)(QString,QString);
    typedef void (QtToQmlConnector::*ProcessingWithIntArgQml)(int);
    //typedef void (QtToQmlConnector::*ProcessingWithoutArgQml)();

private:
    QQmlContext *context;
    AppManager *app_manager;
    [int][int][SimpleExecuter*] a;

    //Qt to QML direction
    QMap<QString,ProcessingWithoutArgumets> baseFuncMap;
    QMap<QString,ProcessingWithOneArgumet> oneArgFuncMap;
    QMap<QString,ProcessingWithTwoArgumets> twoArgsFuncMap;

    //QML to Qt direction
    QMap<QString,ProcessingWithIntArgQml> oneArgIntFuncMapQml;
    QMap<QString,ProcessingWithIntArgQml> oneArgWithoutFuncMapQml;

public:
    explicit QtToQmlConnector(QQmlContext *qmlContext, AppManager *appmanager, QObject *parent = nullptr);

signals:
     // to QML
     void appendFolderButton(QString folder_name);
     void clearFoldersButtons();
     void sendFileNameResponse(QString fileName);
     //to C++
     void requestImageFileName(int index);
     //void requestGetFoldersList();
     void toQtFolderListUpdate();

public slots:
     void acceptCommand(QString command,QStringList params);
     void acceptCommandFromQML(QString command,QtToQmlVariant *params);
     void requestFromQMLWithoutArgs(QString command);
     void requestFromQMLWithIntArg(QString command, int parameter);

};

#endif // QTTOQMLCONNECTOR_H

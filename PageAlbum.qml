import QtQuick 2.7
import "PageAlbumScripts.js" as Scripts

Item {
    id : root
    height : parent.height
    width: parent.width

    property int currentPhoto : 0
    property int currentImageObject: 0 // 0-tip, 1-top
    property int direction: 0  // 0-forward, 1-backward
    property bool startAnimationFlag : true

    Component.onCompleted: {
        MainApplication.requestFromQMLWithIntArg("getFileName",0)
    }

    Connections {
        target : MainApplication

        onSendFileNameResponse : {

            if (startAnimationFlag == true)
            {
                startAnimationFlag = false;
                pic.source = fileName
                //picMoving.start()
            } else {
              if (direction == 0)
                {
                    forwardAnimation.source = fileName
                    forwardAnimation.start()
                } else {
                    backwardAnimation.source = fileName
                    backwardAnimation.start()
                }
            }
        }

    }

    Image {
        anchors.fill: parent
        source: "images/background.jpg"
    }

    Flickable
    {
        id : flick
        property alias source: pic.source
        property int startX: 0
        height: parent.height
        width: parent.width
        anchors.fill: parent
        contentWidth: pic.width * 1.28
        contentHeight: pic.height * 1.28
        Component.onCompleted: {
        }

        Image {
            id : pic
            source : ""
            height:root.height
            width: root.width
        }

        onDragStarted: {
            startX = contentX
        }

        onDragEnded: {
            //if (pic.scale == 1.0)
            //{
                Scripts.moveNextPrev(startX,contentX)
            //}
        }

        PinchArea {
            anchors.fill: parent
            pinch.target: pic
            pinch.minimumScale: 0.1
            pinch.maximumScale: 10
            pinch.dragAxis: Pinch.XAndYAxis
            onSmartZoom:
            {
                if (pinch.scale > 0)
                {
                    pinch.target.scale = scale;
                } else
                {
                    pinch.target.scale = pinch.previousScale
                }
            }
        }
    }


//       NumberAnimation {
//            id : picMoving
//            target: pic
//            property: "x"
//            from: root.width
//            to: 0
//            duration: 100
//        }

//       NumberAnimation {
//           id : picRise
//           target: pic
//           property: "scale"
//           from: 0.0
//           to: 1.1
//           duration: 150
//       }

    SequentialAnimation {
        id : forwardAnimation

        property string source: ""

        NumberAnimation {
            target: pic
            property: "x"
            from: 0
            to: -root.width
            duration: 150
        }

        ScriptAction {
            script : Scripts.doChange(forwardAnimation.source,0)
        }

        NumberAnimation {
            target: pic
            property: "scale"
            from: 0.0
            to: 1.1
            duration: 150
        }

    }

//    function doChange(src)
//    {
//        pic.source = src
//        pic.x = pic.width*1.28/4
//    }

    SequentialAnimation {
        id : backwardAnimation
        property string source: ""

        NumberAnimation {
            target: pic
            property: "x"
            from: 0
            to: root.width
           duration: 150
        }

        ScriptAction {
            script : Scripts.doChange(backwardAnimation.source,1)
        }

        NumberAnimation {
            target: pic
            property: "scale"
            from: 0.0
            to: 1.1
            duration: 150
        }

    }

}



#ifndef SIMPLEEXECUTER_H
#define SIMPLEEXECUTER_H

#include <QObject>
#include <qttoqmlvariant.h>

class SimpleExecuter : public QObject
{
    Q_OBJECT
protected:
    QString command;
    QtToQmlVariant params;

public:
    explicit SimpleExecuter(QString &command, QtToQmlVariant &params, QObject *parent = nullptr);
    virtual void Execute() = 0;

signals:

public slots:
};

#endif // SIMPLEEXECUTER_H

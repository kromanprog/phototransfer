#ifndef QTTOQMLVARIANT_H
#define QTTOQMLVARIANT_H

#include <QObject>
#include <QString>
#include <QString>
#include <QQmlListProperty>
#include <qqmlprivate.h>

class QtToQmlVariant : public QObject
{
    Q_OBJECT
    //Q_PROPERTY(QString string_param_1 READ getString_param_1 WRITE setString_param_1)
    Q_PROPERTY(QQmlListProperty<QString> str_params)
    Q_PROPERTY(QQmlListProperty<int> int_params)


public:
    explicit QtToQmlVariant(QObject *parent = nullptr);

    Q_INVOKABLE void resetStringParams();
    Q_INVOKABLE void pushString(QString);
    Q_INVOKABLE QString getByIndex(int) const;

    Q_INVOKABLE void resetIntParams();
    Q_INVOKABLE void pushInt(int);
    Q_INVOKABLE int getByIndex(int) const;

    Q_INVOKABLE int getStrParamsCount() const;
    Q_INVOKABLE int getIntParamsCount() const;

private:
    QList<QString> m_str_params;
    QList<int> m_int_params;

signals:

public slots:
};

#endif // QTTOQMLVARIANT_H

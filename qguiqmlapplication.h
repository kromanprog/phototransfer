#ifndef QGUIQMLAPPLICATION_H
#define QGUIQMLAPPLICATION_H

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "imagemanager.h"
#include "QtToQmlConnector.h"
#include "QQmlContext"
#include "appmanager.h"


class QGuiQmlApplication : public QGuiApplication
{
private:
    ImageManager *iManager;
    QtToQmlConnector *qtqmlconnector;
    QQmlContext *context;
    QQmlApplicationEngine *engine;
    AppManager *appmanager;
public:
    QGuiQmlApplication(int &argc, char **argv);
    ~QGuiQmlApplication();
    bool Init();
};

#endif // QGUIQMLAPPLICATION_H

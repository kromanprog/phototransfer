#include <QVariant>
#include "QtToQmlConnector.h"
#include "qttoqmlvariant.h"
#include "qstringlist.h"


QtToQmlConnector::QtToQmlConnector(QQmlContext *qmlContext, AppManager *appmanager, QObject *parent) : QObject(parent)
{
    context = qmlContext;
    context->setContextProperty("FolderList",QVariant::fromValue(appmanager->folders));
    context->setContextProperty("MainApplication",this);

    baseFuncMap["clearFoldersButtons"] = &QtToQmlConnector::clearFoldersButtons;    // toQML
    baseFuncMap["GetFolderList"] = &QtToQmlConnector::toQtFolderListUpdate;         // toQt
    oneArgFuncMap["appendFolderButton"] = &QtToQmlConnector::appendFolderButton;    //toQML
    oneArgFuncMap["sendFileNameResponse"] = &QtToQmlConnector::sendFileNameResponse;//toQML
    oneArgIntFuncMapQml["getFileName"] = &QtToQmlConnector::requestImageFileName;   //toQt

}

void QtToQmlConnector::acceptCommand(QString command, QStringList params)
{
    switch(params.count())
    {
    case 0:
    {
        if (baseFuncMap.contains(command)) (   this->*(baseFuncMap[command])   ) ();
        break;
    }
    case 1:
    {
        if (oneArgFuncMap.contains(command))
        {
            ( this->*(oneArgFuncMap[command]) ) (params[0]);
        }
        break;
    }
    case 2:
    {
        if (twoArgsFuncMap.contains(command)) ( this->*(twoArgsFuncMap[command])) (params[0],params[1]);
        break;
    }
    default :
    {

    }

    }
}

void QtToQmlConnector::acceptCommandFromQML(QString command, QtToQmlVariant *params)
{

    QtToQmlVariant *pParam = dynamic_cast<QtToQmlVariant*>(params);
    if (!p)
    {
        return;
    }

    (this->*(baseFuncMap["GetFolderList"]))();
}




void QtToQmlConnector::requestFromQMLWithoutArgs(QString command)
{
    command = "";
}

void QtToQmlConnector::requestFromQMLWithIntArg(QString command, int parameter)
{
    if (oneArgIntFuncMapQml.contains(command)) ( this->*(oneArgIntFuncMapQml[command])) (parameter);
}

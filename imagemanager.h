#ifndef IMAGEMANAGER_H
#define IMAGEMANAGER_H

#include <QObject>

class ImageManager : public QObject
{
    Q_OBJECT
private:
    QStringList urlList;
    int currentIndex = 0;
    void findImagesRecursively(QString rootDir);


public:
    explicit ImageManager(QObject *parent = nullptr);

signals:
    void fileNameResponse(QString,QStringList);

public slots:
    void getFileName(int index);

};

#endif // IMAGEMANAGER_H

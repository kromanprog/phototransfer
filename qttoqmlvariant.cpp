#include "qttoqmlvariant.h"

QtToQmlVariant::QtToQmlVariant(QObject *parent) : QObject(parent)
{

}

void QtToQmlVariant::resetStringParams()
{
    m_str_params.clear();
}

void QtToQmlVariant::pushString(QString s)
{
    m_str_params.push_back(s);
}

QString QtToQmlVariant::getByIndex(int index)
{
    if (index < m_str_params.count())
        return m_str_params.at(index);
    else
        return "";
}


void QtToQmlVariant::resetIntParams()
{
    m_int_params.clear();
}

void QtToQmlVariant::pushInt(int i)
{
    m_int_params.push_back(i);
}

int QtToQmlVariant::getByIndex(int i) const
{
    if (i < m_int_params.count() )
        return m_int_params[i];
    else
        return -1;
}

int QtToQmlVariant::getStrParamsCount() const
{
    return m_str_params.count();
}

int QtToQmlVariant::getIntParamsCount() const
{
    return m_int_params.count();
}




#ifndef CONNECTOR_H
#define CONNECTOR_H
#include <QTcpSocket>
#include <QHostAddress>
#include <QHostAddress>
#include <QString>

class Connector : public QObject
{
    Q_OBJECT
public:
    Connector(const QString& ipaddress,int port);
    ~Connector();
    bool sendData(QByteArray& ba) const;
signals:
    void dataReadyToRead(QString);

private slots:
    void readData();

private:
    QTcpSocket *soc;
    QHostAddress serverAddress;
    int serverPort;
};

#endif // CONNECTOR_H

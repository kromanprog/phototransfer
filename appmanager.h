#ifndef APPMANAGER_H
#define APPMANAGER_H

#include <QObject>

class QStringList;

class AppManager : public QObject
{
    Q_OBJECT
public:
    explicit AppManager(QObject *parent = nullptr);
    ~AppManager();
    QStringList folders;
public slots:
    void folderListUpdate();
signals:

};

#endif // APPMANAGER_H

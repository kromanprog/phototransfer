import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

ApplicationWindow {
    id : app
    visible: true
    width: 480
    height: 640
    property alias app: app
    title: qsTr("photoTransfer")

    property int activePageCount: 0

    background: Image {
        source: "images/background.jpg"
    }

    Item {
        anchors.fill : parent

        id : launcherList

            Image {
              id: image
            x: 83
            y: 181
            width: 150
            height: 150
            source: "images/photo.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    var page = pageComponent.createObject(pageContainer, { exampleUrl: Qt.resolvedUrl("dc/declarative-camera.qml") })
                    page.show()}
            }
        }

            Image {
                id: image1
                x: 270
                y: 181
                width: 150
                height: 150
                source: "images/album.png"
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        var page = pageComponent.createObject(pageContainer, { exampleUrl: Qt.resolvedUrl("PageAlbum.qml") })
                        page.show()}
                }
            }

            Image {
                id: image2
                x: 83
                y: 347
                width: 150
                height: 150
                source: "images/folders.png"
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        var page = pageComponent.createObject(pageContainer, { exampleUrl: Qt.resolvedUrl("PageFolders.qml") })
                        page.show()}
                }

            }

            Image {
                id: image3
                x: 270
                y: 353
                width: 150
                height: 150
                source: "images/settings.png"
            }

            Image {
                id: image4
                x: 396
                y: 20
                width: 80
                height: 74
                source: "images/applicationexit.png"
                MouseArea {
                    anchors.fill: parent
                    onClicked : {

                    }
                }
            }

        }

        Item {
            id: pageContainer
            anchors.fill: parent
        }

        Component {
            id: pageComponent
            Rectangle {
                id: page
                clip: true
                property url exampleUrl
                width: parent.width
                height: parent.height - bar.height
                color: "white"
                MouseArea{
                    //Eats mouse events
                    anchors.fill: parent
                }
                Loader{
                    focus: true
                    source: parent.exampleUrl
                    anchors.fill: parent
                }

                x: -width

                function show() {
                    showAnim.start()
                }

                function exit() {
                    exitAnim.start()
                }

                ParallelAnimation {
                    id: showAnim
                    ScriptAction {
                        script: activePageCount++
                    }
                    NumberAnimation {
                        target: launcherList
                        property: "opacity"
                        from: 1.0
                        to: 0.0
                        duration: 500
                    }
                    NumberAnimation {
                        target: launcherList
                        property: "scale"
                        from: 1.0
                        to: 0.0
                        duration: 500
                    }
                    NumberAnimation {
                        target: page
                        property: "x"
                        from: page.width
                        to: 0
                        duration: 300
                    }
                }
                SequentialAnimation {
                    id: exitAnim

                    ScriptAction {
                        script: activePageCount--
                    }

                    ParallelAnimation {
                        NumberAnimation {
                            target: launcherList
                            property: "opacity"
                            from: 0.0
                            to: 1.0
                            duration: 300
                        }
                        NumberAnimation {
                            target: launcherList
                            property: "scale"
                            from: 0.0
                            to: 1.0
                            duration: 300
                        }
                        NumberAnimation {
                            target: page
                            property: "x"
                            from: 0
                            to: page.width
                            duration: 300
                        }
                    }

                    ScriptAction {
                        script: page.destroy()
                    }
                }
            }
        }
        Rectangle {
            id: bar
            visible: height > 0
            anchors.bottom: parent.bottom
            width: parent.width
            height: activePageCount > 0 ? 40 : 0

            Behavior on height {
                NumberAnimation {
                    duration: 300
                }
            }
            border.width: 4
            border.color: "#027408"

            Image { id : barbackground
                anchors.fill: parent
                source: "images/bar_background.jpg"

            }

            Image {
                id: back
                source: "images/back.png"
                anchors.verticalCenter: parent.verticalCenter
                anchors.verticalCenterOffset: 2
                anchors.left: parent.left
                anchors.leftMargin: 16

                height : parent.height
                width : height

                MouseArea {
                    id: mouse
                    hoverEnabled: true
                    anchors.centerIn: parent
                    width: 38
                    height: 31
                    anchors.verticalCenterOffset: -1
                    enabled: activePageCount > 0
                    onClicked: {
                        pageContainer.children[pageContainer.children.length - 1].exit()
                    }
                    Rectangle {
                        anchors.fill: parent
                        opacity: mouse.pressed ? 1 : 0
                        Behavior on opacity { NumberAnimation{ duration: 100 }}
                        gradient: Gradient {
                            GradientStop { position: 0 ; color: "#22000000" }
                            GradientStop { position: 0.2 ; color: "#11000000" }
                        }
                        border.color: "darkgray"
                        antialiasing: true
                        radius: 4
                    }
                }
            }
        }
    //}



}
